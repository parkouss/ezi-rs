# ezi-rs

An attempt to write the ezi parser in rust, using the
[nom](https://github.com/Geal/nom) parsor combinator library.

# build

If you don't have rust installed yet, use [rustup](https://rustup.rs/).

Then you can run the command:

`cargo build`

# run the unit tests

`cargo test`
