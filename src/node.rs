pub type DottedName = Vec<String>;

#[derive(Debug, PartialEq)]
pub struct Struct {
    pub name: String,
    pub content: Vec<StructContent>,
    pub properties: Vec<StructProperty>,
    pub docstring: Option<String>,
    pub bases: Vec<DottedName>,
}

#[derive(Debug, PartialEq)]
pub enum StructContent {
    Field(Field),
    Struct(Struct),
    Enum(Enum),
    Variant(Variant),
}

#[derive(Debug, PartialEq)]
pub enum StructProperty {
    CustomValidate(bool),
}

#[derive(Debug, PartialEq)]
pub struct Field {
    pub name: String,
    pub ftype: FieldType,
}

#[derive(Debug, PartialEq)]
pub enum FieldType {
    String,
    Int,
    Uint,
    Double,
    Bool,
    List(Box<FieldType>),
    Map(Box<FieldType>),
    Optional(Box<FieldType>),
    RefValue(Box<FieldType>),
    Ref(DottedName),
}

#[derive(Debug, PartialEq)]
pub struct Enum {
    pub name: String,
    pub values: Vec<String>,
    pub docstring: Option<String>,
}

#[derive(Debug, PartialEq)]
pub struct Variant {
    pub name: String,
    pub values: Vec<Field>,
    pub docstring: Option<String>,
}

#[derive(Debug, PartialEq)]
pub struct Node {
    pub name: String,
    pub bases: Vec<DottedName>,
    pub docstring: Option<String>,
    pub concrete: bool,
}

#[derive(Debug, PartialEq)]
pub enum ModuleDef {
    Struct(Struct),
    Enum(Enum),
    Variant(Variant),
    Node(Node),
}

#[derive(Debug, PartialEq)]
pub struct Module {
    pub imports: Vec<String>,
    pub definitions: Vec<ModuleDef>,
}
