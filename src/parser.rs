use nom::IResult;
use nom::bytes::complete::{tag, escaped_transform, is_a};
use nom::character::complete::{multispace1, alphanumeric1, char, none_of,
                               not_line_ending, alpha1};
use nom::sequence::{preceded, tuple, delimited};
use nom::branch::alt;
use nom::multi::{many0, many1, fold_many0, separated_nonempty_list};
use nom::combinator::{opt, cut, map, all_consuming};
use node;
use nom::error::{context};

pub use nom::error::{convert_error, VerboseError};
pub use nom::Err;

pub type Input<'a> = &'a str;

/// Our custom parser result type work on &str input and use verbose errors.
pub type Result<'a, O> = IResult<Input<'a>, O, VerboseError<Input<'a>>>;


/// strip whitespaces and comment
fn ws(input: Input) -> Result<()> {
    fold_many0(
        alt((
            // spaces
            multispace1,
            // single line comment
            preceded(tag("//"), not_line_ending)
        )),
        // just discard what was parsed
        (),
        |acc, _| { acc }
    )(input)
}

/// function to parse an identifier, such as "foo1_bar"
fn parse_identifier(input: Input) -> Result<String> {
    let (input, _) = ws(input)?;
    let (input, id_base) = alt((alpha1, is_a("_")))(input)?;
    let (input, id_rest) = many0(alt((alphanumeric1, is_a("_"))))(input)?;
    let str_length =
        id_rest.iter().fold(id_base.len(), |acc, i| acc + i.len());
    let mut id = String::new();
    id.reserve_exact(str_length);
    id.push_str(id_base.into());
    for r in id_rest {
        id.push_str(r.into());
    }
    Ok((input, id))
}

fn parse_import_line(input: Input) -> Result<Vec<String>> {
    let separator = preceded(ws, char(','));
    delimited(
        delimited(ws, tag("import"), multispace1),
        separated_nonempty_list(separator, parse_identifier),
        preceded(ws, char(';'))
    )(input)
}

fn parse_imports(input: Input) -> Result<Vec<String>> {
    let (input, imports) = many0(parse_import_line)(input)?;
    let mut res = Vec::new();
    for mut i in imports {
        res.append(&mut i);
    }
    Ok((input, res))
}

fn parse_dotted_name(input: Input) -> Result<node::DottedName> {
    let separator = preceded(ws, char('.'));
    separated_nonempty_list(separator, parse_identifier)(input)
}

/// parse identifier and «bases», that is « Stuff : node.Foo, Other
fn parse_identifier_and_bases(input: Input) -> Result<(String, Vec<node::DottedName>)>
{
    let separator = preceded(ws, char(','));
    let parse_bases = preceded(ws, preceded(char(':'), cut(
        separated_nonempty_list(separator, parse_dotted_name)
    )));

    tuple((
        parse_identifier,
        map(opt(parse_bases), |r| match r {
            Some(b) => b,
            None => Vec::new()
        })
    ))(input)
}

/// A macro that match a definition, that is something with:
///
/// "some optional docstring"
/// TAG PARSER1 { PARSER2 }
macro_rules! parse_def {
    ( $tag: tt, $parser1: expr, $parser2: expr ) => {
        tuple((
            parse_docstring,
            preceded(ws, preceded(tag($tag), cut($parser1))),
            cut(delimited(
                preceded(ws, char('{')),
                $parser2,
                preceded(ws, char('}'))
            ))
        ))
    }
}

fn parse_enum(input: Input) -> Result<node::Enum> {
    let separator = preceded(ws, char(','));
    let enum_value = alt((parse_string, parse_identifier));
    map(
        parse_def!("enum", parse_identifier,
                   separated_nonempty_list(separator, enum_value)),
        |(docstring, name, values)| node::Enum{name, values, docstring}
    )(input)
}

fn parse_variant(input: Input) -> Result<node::Variant> {
    map(
        parse_def!("variant", parse_identifier, many1(parse_field)),
        |(docstring, name, values)| node::Variant{name, values, docstring}
    )(input)
}

/// parse a string, surrounded by double quotes
fn parse_string(input: Input) -> Result<String> {
    // allow to escape some characters
    let transform = |s| {
        alt((
            map(char('n'), |_| "\n"),
            map(char('\\'), |_| "\\"),
            map(char('"'), |_| "\""),
        ))(s)
    };

    preceded(ws, context(
        "string",
        delimited(
            char('"'),
            // «cut» the string content to fail on the alt parser
            cut(escaped_transform(none_of("\\\""), '\\', transform)),
            char('"')
        )
    ))(input)
}

/// a docstring is a succession of strings - or nothing
fn parse_docstring(input: Input) -> Result<Option<String>> {
    let (input, strs) = many0(parse_string)(input)?;
    let res = if strs.is_empty() {
        None
    } else {
        let mut docstring = String::new();
        // reserve the exact size for the string
        docstring.reserve_exact(strs.iter().fold(0, |sum, s| sum + s.len()));
        // and fill the string
        for s in strs {
            docstring.push_str(&s);
        }
        Some(docstring)
    };
    Ok((input, res))
}

fn parse_field_type(input: Input) -> Result<node::FieldType> {
    // template type, such at list < ... >
    let parse_tpl = |name| {
        // starts with a known tag
        preceded(
            tag(name),
            // then we use cut to signal an error as a failure
            cut(delimited(
                preceded(ws, char('<')),
                parse_field_type,
                preceded(ws, char('>'))
            )))
    };
    // parse every known type
    preceded(ws, alt((
        map(tag("int"), |_| node::FieldType::Int),
        map(tag("uint"), |_| node::FieldType::Uint),
        map(tag("string"), |_| node::FieldType::String),
        map(tag("double"), |_| node::FieldType::Double),
        map(tag("bool"), |_| node::FieldType::Bool),
        map(parse_tpl("list"), |subtype| node::FieldType::List(Box::new(subtype))),
        map(parse_tpl("map"), |subtype| node::FieldType::Map(Box::new(subtype))),
        map(parse_tpl("optional"), |subtype| node::FieldType::Optional(Box::new(subtype))),
        map(parse_tpl("refvalue"), |subtype| node::FieldType::RefValue(Box::new(subtype))),
        map(parse_dotted_name, |names| node::FieldType::Ref(names)),
    )))(input)
}

fn parse_field(input: Input) -> Result<node::Field> {
    map(tuple((
        parse_field_type,
        parse_identifier,
        preceded(ws, char(';'))
    )), |(ftype, name, _)| node::Field{name, ftype})(input)
}

fn parse_struct_properties_custom_validate(input: Input) -> Result<node::StructProperty> {
    map(tuple((
        preceded(ws, tag("customValidate")),
        preceded(ws, char(':')),
        preceded(ws, tag("true"))
    )), |(_,_,_)| node::StructProperty::CustomValidate(true))(input)
}

fn parse_struct_properties(input: Input) -> Result<Vec<node::StructProperty>> {
    let (input, props) = opt(preceded(ws, tag("properties")))(input)?;
    match props {
        Some(_) => {
            delimited(
                preceded(ws, char('{')),
                many1(parse_struct_properties_custom_validate),
                preceded(ws,char('}'))
            )(input)
        }
        None => Ok((input, Vec::new()))
    }
}

fn parse_struct(input: Input) -> Result<node::Struct> {
    map(
        parse_def!("struct", parse_identifier_and_bases, tuple((
            parse_struct_properties,
            many0(alt((
                map(parse_field, |f| node::StructContent::Field(f)),
                map(parse_struct, |s| node::StructContent::Struct(s)),
                map(parse_enum, |e| node::StructContent::Enum(e)),
                map(parse_variant, |v| node:: StructContent::Variant(v))
            )))
        ))),
        |(docstring, (name, bases), (properties, content))| node::Struct{
            name,
            content,
            properties,
            docstring,
            bases,
        }
    )(input)
}

fn parse_node(input: Input) -> Result<node::Node> {
    let parse_node_content = ws;
    alt((
        map(
            parse_def!("node", parse_identifier_and_bases, parse_node_content),
            |(docstring, (name, bases), _)|
            node::Node{name, bases, docstring, concrete: true}
        ),
        map(
            parse_def!("interface", parse_identifier_and_bases, parse_node_content),
            |(docstring, (name, bases), _)|
            node::Node{name, bases, docstring, concrete: false}
        )
    ))(input)
}

pub fn parse_module(input: Input) -> Result<node::Module> {
    let module_def = alt((
        map(parse_struct, |s| node::ModuleDef::Struct(s)),
        map(parse_enum, |e| node::ModuleDef::Enum(e)),
        map(parse_variant, |v| node::ModuleDef::Variant(v)),
        map(parse_node, |n| node::ModuleDef::Node(n)),
    ));
    map(tuple((
        parse_imports,
        many1(module_def),
        all_consuming(ws)
    )), |(imports, definitions, _)| node::Module {imports, definitions})(input)
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_parse_ws() {
        assert_eq!(ws("// foo bar\n   "), Ok(("", ())));
    }

    #[test]
    fn test_parse_identifier() {
        let (rest, result) = parse_identifier("foo_bar1 other").unwrap();
        assert_eq!(result, "foo_bar1");
        assert_eq!(rest, " other");
    }

    #[test]
    fn test_parse_docstring() {
        let (rest, result) = parse_docstring(" \"hello\" \" there\" ").unwrap();
        let result = result.unwrap();
        assert_eq!(result, "hello there");
        assert_eq!(result.capacity(), result.len());
        assert_eq!(rest, " ");
    }

    #[test]
    fn test_parse_imports() {
        let (rest, result) = parse_imports("import foo;\n\n import bar, stuff;  ").unwrap();
        assert_eq!(result, vec!["foo", "bar", "stuff"]);
        assert_eq!(rest, "  ");
    }

    #[test]
    fn test_parse_field_types() {
        let parse_type = |type_str| parse_field_type(type_str).unwrap().1;

        assert_eq!(parse_type("int"), node::FieldType::Int);
        assert_eq!(parse_type("uint"), node::FieldType::Uint);
        assert_eq!(parse_type("bool"), node::FieldType::Bool);
        assert_eq!(parse_type("double"), node::FieldType::Double);
        assert_eq!(parse_type("refvalue<int>"),
                   node::FieldType::RefValue(Box::new(node::FieldType::Int)));
        assert_eq!(parse_type("list< map< string  >>"),
                   node::FieldType::List(
                       Box::new(node::FieldType::Map(
                           Box::new(node::FieldType::String)))));

        assert_eq!(parse_type("Foo"),
                   node::FieldType::Ref(vec!["Foo".to_owned()]));
        assert_eq!(parse_type("Foo.Bar"),
                   node::FieldType::Ref(vec!["Foo".to_owned(), "Bar".to_owned()]));
    }

    #[test]
    fn test_parse_node() {
        let (_, n) = parse_node("node Foo {}").unwrap();
        assert_eq!(n.name, "Foo");
        assert_eq!(n.concrete, true);

        let (_, n) = parse_node("interface Foo1 {}").unwrap();
        assert_eq!(n.name, "Foo1");
        assert_eq!(n.concrete, false);
    }
}
