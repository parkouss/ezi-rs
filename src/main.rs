extern crate nom;

mod parser;
mod node;

use std::io::{self, Read};

fn main() {
    let mut buffer = String::new();
    io::stdin().lock().read_to_string(&mut buffer).unwrap();

    let result = parser::parse_module(&buffer);

    match result {
        Ok(r) => { println!("{:?}", r); },
        Err(parser::Err::Error(e)) | Err(parser::Err::Failure(e)) => {
            println!("{}", parser::convert_error(&buffer, e));
        },
        Err(e) => { println!("{:?}", e); },
    }
}
